# README #

To run the object tracking robot simulation, please follow the following steps:

1. git clone https://jj664@bitbucket.org/jj664/pde4426_cw2.git in your ros/catkin_ws/src folder OR Copy the PDE4426_CW2_jaywin folder to your ros/catkin_ws/src folder

2. Run catkin_make

3. Launch the world by running: roslaunch my_worlds tessel.launch

4. Spawn the robot in the world using: roslaunch m2wr_description spawn.launch

5. Run the object tracking program:  rosrun teleop_twist_keyboard teleop_twist_keyboard.py 


This program uses a modified version of teleop_twist_keyboard.py 

Please make sure you do not have any other version of teleop_twist_keyboard.py installed.